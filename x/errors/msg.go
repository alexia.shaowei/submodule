package errors

import (
	"fmt"
	"io"
)

type withMsg struct {
	message string
	orgErr  error
}

func (ref *withMsg) Format(stat fmt.State, rn rune) {
	switch rn {
	case cst_CHAR_V:
		if stat.Flag(cst_CHAR_PLUS) {
			fmt.Fprintf(stat, "%+v\n", ref.OrgErr())
			io.WriteString(stat, ref.message)

			return
		}

		fallthrough

	case cst_CHAR_S, cst_CHAR_Q:
		io.WriteString(stat, ref.Error())

	}
}

func (ref *withMsg) Error() string {
	return ref.message + cst_COLON + ref.orgErr.Error()
}

func (ref *withMsg) OrgErr() error {
	return ref.orgErr
}

func (ref *withMsg) Unwrap() error {
	return ref.orgErr
}
