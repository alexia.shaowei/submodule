package errors

import (
	"fmt"
	"io"
)

type errBase struct {
	message string
	*stack
}

func (ref *errBase) Error() string {
	return ref.message
}

func (ref *errBase) Format(s fmt.State, rn rune) {
	switch rn {
	case cst_CHAR_Q:
		fmt.Fprintf(s, "%q", ref.message)

	case cst_CHAR_V:
		if s.Flag(cst_CHAR_PLUS) {
			io.WriteString(s, ref.message)
			ref.stack.Format(s, rn)

			return
		}

		fallthrough

	case cst_CHAR_S:
		io.WriteString(s, ref.message)

		// default:

	}
}
