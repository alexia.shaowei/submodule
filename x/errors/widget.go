package errors

import (
	"runtime"
	"strings"
)

func funcName(name string) string {
	i := strings.LastIndex(name, cst_SLASH)

	i = strings.Index(name[i+1:], cst_DOT)

	return name[i+1:]
}

func callers() *stack {
	var depthPtr [cst_DEPTH]uintptr

	n := runtime.Callers(3, depthPtr[:])

	var st stack = depthPtr[0:n]
	return &st
}
