package errors

import (
	go113err "errors"
)

func Is(err, target error) bool {
	return go113err.Is(err, target)
}

func As(err error, target interface{}) bool {
	return go113err.As(err, target)
}

func Unwrap(err error) error {
	return go113err.Unwrap(err)
}
