package errors

const (
	cst_BRACKET_L   = "["
	cst_BRACKET_R   = "]"
	cst_SLASH       = "/"
	cst_DOT         = "."
	cst_PLUS        = "+"
	cst_COLON       = ":"
	cst_SPACE       = " "
	cst_NEWLINE     = "\n"
	cst_NEWLINE_TAB = "\n\t"
)

const (
	cst_CHAR_HASHTAG = '#'
	cst_CHAR_PLUS    = '+'
	cst_CHAR_S       = 's'
	cst_CHAR_D       = 'd'
	cst_CHAR_N       = 'n'
	cst_CHAR_V       = 'v'
	cst_CHAR_Q       = 'q'
)

const (
	cst_UNKNOWN   = "unknown"
	cst_AMBIGUOUS = "ambiguous"
	cst_UNDEFINED = "undefined"
)

const (
	cst_DEPTH = 32
)
