package errors

import "fmt"

func New(message string) error {
	return &errBase{
		message: message,
		stack:   callers(),
	}
}

func Errorf(message string, args ...interface{}) error {
	return &errBase{
		message: fmt.Sprintf(message, args...),
		stack:   callers(),
	}
}

func WithStack(err error) error {
	if err == nil {
		return nil
	}

	return &wrapStack{
		err,
		callers(),
	}
}

func Wrap(err error, message string) error {
	if err == nil {
		return nil
	}
	err = &withMsg{
		orgErr:  err,
		message: message,
	}
	return &wrapStack{
		err,
		callers(),
	}
}

func Wrapf(err error, message string, args ...interface{}) error {
	if err == nil {
		return nil
	}
	err = &withMsg{
		orgErr:  err,
		message: fmt.Sprintf(message, args...),
	}
	return &wrapStack{
		err,
		callers(),
	}
}

func WithMessage(err error, message string) error {
	if err == nil {
		return nil
	}
	return &withMsg{
		orgErr:  err,
		message: message,
	}
}

func WithMessagef(err error, message string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	return &withMsg{
		orgErr:  err,
		message: fmt.Sprintf(message, args...),
	}
}

type orgErr interface {
	OrgErr() error
}

func OrgErr(err error) error {

	for err != nil {
		orgErr, ok := err.(orgErr)
		if !ok {
			break
		}
		err = orgErr.OrgErr()
	}

	return err
}
