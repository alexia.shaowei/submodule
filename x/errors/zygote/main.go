package main

import (
	"fmt"

	"gitlab.com/alexia.shaowei/submodule/x/errors"
)

func main() {

	_, err := ReadCfg()
	if err != nil {
		fmt.Printf("org error: %T %v\n", errors.OrgErr(err), errors.OrgErr(err))
		fmt.Printf("stack trace:\n%+v\n", err)
	}
	fmt.Println()
	fmt.Println()

	fmt.Println("************* WRAP   ***************************************************")
	Wrap()
	fmt.Println("************* Unwrap ***************************************************")
	Unwrap()
	fmt.Println("************* Stack Trace  *********************************************")
	StackTrace()

}
