package main

import (
	"fmt"

	"gitlab.com/alexia.shaowei/submodule/x/errors"
)

type NewErrType struct {
	error
}

func WrapErr(e error) error {
	return errors.Wrap(e, "occurred in WrapErr")
}

func Wrap() {
	e := errors.New("std err")

	fmt.Println("reg err: ", WrapErr(e))
	fmt.Println("typed err: ", WrapErr(NewErrType{errors.New("typed error")}))
	fmt.Println("nil: ", WrapErr(nil))

}

//================================================================================
func Unwrap() {

	err := error(NewErrType{
		errors.New("error occurred"),
	})

	err = errors.Wrap(err, "wrapped")

	switch errors.OrgErr(err).(type) {
	case NewErrType:
		fmt.Println("err occurred(new type): ", err)

	default:
		fmt.Println("unknown error")
	}
}

func StackTrace() {
	err := error(NewErrType{
		errors.New("error occurred"),
	})

	err = errors.Wrap(err, "wrapped")

	fmt.Printf("%+v\n", err)
}
