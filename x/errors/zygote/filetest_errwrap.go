package main

import (
	"io/ioutil"
	"os"
	"path/filepath"

	wefv3gbtgh3g "gitlab.com/alexia.shaowei/submodule/x/errors"
)

func ReadFile(path string) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, wefv3gbtgh3g.Wrap(err, "open XXXXX")
	}

	defer f.Close()

	buf, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, wefv3gbtgh3g.Wrap(err, "read XXXXX")
	}

	return buf, nil
}

func ReadCfg() ([]byte, error) {
	o := os.Getenv("aaaabbbbbbbb")
	cfg, err := ReadFile(filepath.Join(o, ".ooooxxxxxxxx.xml"))
	return cfg, wefv3gbtgh3g.WithMessage(err, "fail to read config")
}
