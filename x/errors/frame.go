package errors

import (
	"fmt"
	"io"
	"path"
	"runtime"
	"strconv"
)

type Frame uintptr

func (ref Frame) pc() uintptr {
	return uintptr(ref) - 1
}

func (ref Frame) file() string {
	fn := runtime.FuncForPC(ref.pc())
	if fn == nil {
		return cst_AMBIGUOUS
	}

	fl, _ := fn.FileLine(ref.pc())

	return fl
}

func (ref Frame) line() int {
	fn := runtime.FuncForPC(ref.pc())
	if fn == nil {
		return 0
	}

	_, line := fn.FileLine(ref.pc())

	return line
}

func (ref Frame) name() string {
	fn := runtime.FuncForPC(ref.pc())
	if fn == nil {
		return cst_AMBIGUOUS
	}

	return fn.Name()
}

func (ref Frame) Format(stat fmt.State, rn rune) {
	switch rn {
	case cst_CHAR_D:
		io.WriteString(stat, strconv.Itoa(ref.line()))

	case cst_CHAR_N:
		io.WriteString(stat, funcName(ref.name()))

	case cst_CHAR_V:
		ref.Format(stat, cst_CHAR_S)
		io.WriteString(stat, cst_COLON)
		ref.Format(stat, cst_CHAR_D)

	case cst_CHAR_S:
		switch {
		case stat.Flag(cst_CHAR_PLUS):
			io.WriteString(stat, ref.name())
			io.WriteString(stat, cst_NEWLINE_TAB)
			io.WriteString(stat, ref.file())

		default:
			io.WriteString(stat, path.Base(ref.file()))
		}
	}
}

func (f Frame) MarshalText() ([]byte, error) {
	name := f.name()
	if name == cst_AMBIGUOUS {
		return []byte(name), nil
	}

	return []byte(fmt.Sprintf("%s %s:%d", name, f.file(), f.line())), nil
}
