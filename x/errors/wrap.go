package errors

import (
	"fmt"
	"io"
)

type wrapStack struct {
	error
	*stack
}

func (ref *wrapStack) OrgErr() error {
	return ref.error
}

func (ref *wrapStack) Unwrap() error {
	return ref.error
}

func (ref *wrapStack) Format(stat fmt.State, rn rune) {
	switch rn {
	case cst_CHAR_Q:
		fmt.Fprintf(stat, "%q", ref.Error())

	case cst_CHAR_V:
		if stat.Flag(cst_CHAR_PLUS) {
			fmt.Fprintf(stat, "%+v", ref.OrgErr())
			ref.stack.Format(stat, rn)

			return
		}

		fallthrough

	case cst_CHAR_S:
		io.WriteString(stat, ref.Error())

	}
}
