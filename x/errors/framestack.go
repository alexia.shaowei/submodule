package errors

import (
	"fmt"
	"io"
)

type FrameTrace []Frame

func (ref FrameTrace) Format(stat fmt.State, rn rune) {
	switch rn {
	case cst_CHAR_S:
		ref.fmtSlice(stat, rn)

	case cst_CHAR_V:
		switch {
		case stat.Flag(cst_CHAR_HASHTAG):
			fmt.Fprintf(stat, "%#v", []Frame(ref))

		case stat.Flag(cst_CHAR_PLUS):
			for _, f := range ref {
				io.WriteString(stat, cst_NEWLINE)
				f.Format(stat, rn)
			}

		default:
			ref.fmtSlice(stat, rn)
		}

	}
}

func (ref FrameTrace) fmtSlice(stat fmt.State, rn rune) {
	io.WriteString(stat, cst_BRACKET_L)

	for i, f := range ref {
		if i > 0 {
			io.WriteString(stat, cst_SPACE)
		}

		f.Format(stat, rn)
	}

	io.WriteString(stat, cst_BRACKET_R)
}

type stack []uintptr

func (ref *stack) Format(stat fmt.State, rn rune) {
	switch rn {
	case cst_CHAR_V:
		switch {
		case stat.Flag(cst_CHAR_PLUS):
			for _, pc := range *ref {
				f := Frame(pc)
				fmt.Fprintf(stat, "\n%+v", f)
			}
		}
	}
}

func (ref *stack) StackTrace() FrameTrace {
	frmL := make([]Frame, len(*ref))

	for i := 0; i < len(frmL); i++ {
		frmL[i] = Frame((*ref)[i])
	}

	return frmL
}
