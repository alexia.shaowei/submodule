package bncalculator

import (
	"strconv"
)

func ktsProduct(x, y string) string {
	x, y = alignNil(x, y)
	l := len(x)
	if len(x) == 1 {
		return strconv.Itoa(int((x[0] - HEX0) * (y[0] - HEX0)))
	}

	xL := x[:l/2]
	xR := x[l/2:]

	yL := y[:l/2]
	yR := y[l/2:]

	zone0 := ktsProduct(xL, yL)
	zone1 := ktsProduct(xR, yR)
	zone2 := ktsProduct(ktsSum(xL, xR), ktsSum(yL, yR))
	zone3 := ktsDifference(zone2, ktsSum(zone0, zone1))

	zone0 = paddingSxNil(zone0, 2*(l-l/2))
	zone3 = paddingSxNil(zone3, l-l/2)

	product := ktsSum(ktsSum(zone0, zone1), zone3)

	return product
}

func ktsSum(x, y string) string {
	return trimIntegerNil(sum(x, y))
}

func ktsDifference(x, y string) string {
	return trimIntegerNil(difference(x, y))
}
