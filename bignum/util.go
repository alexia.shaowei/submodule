package bignum

func min(x, y int32) int32 {
	if x >= y {
		return y
	}

	return x
}

func alignScale(x bignumber, y bignumber) (bignumber, bignumber) {
	x.initIfNil()
	y.initIfNil()

	if x.exp == y.exp {
		return x, y
	}

	bScale := min(x.exp, y.exp)
	if bScale != x.exp {
		return x.align(bScale), y
	}

	return x, y.align(bScale)
}
