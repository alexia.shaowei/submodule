package mapflect

import (
	"fmt"
	"reflect"
	"strconv"
)

func (ref *marshaler) Marshal(in interface{}) error {
	outVal := reflect.ValueOf(ref.output.out).Elem()

	return ref.marshal("", in, outVal)
}

func (ref *marshaler) marshal(fieldname string, in interface{}, outVal reflect.Value) error {
	var inVal reflect.Value
	if in != nil {
		inVal = reflect.ValueOf(in)

		if inVal.Kind() == reflect.Ptr && inVal.IsNil() {
			in = nil
		}
	}

	if in == nil {
		outVal.Set(reflect.Zero(outVal.Type()))
		return nil
	}

	if !inVal.IsValid() {
		outVal.Set(reflect.Zero(outVal.Type()))
		return nil
	}

	var err error
	outValKind := getNumKind(outVal)

	switch outValKind {
	case reflect.Bool:
		err = ref.reflectBool(in, outVal)
	case reflect.Int:
		err = ref.reflectInt(in, outVal)
	case reflect.Uint:
		err = ref.reflectUint(in, outVal)
	case reflect.Float32:
		err = ref.reflectFloat(in, outVal)
	case reflect.String:
		err = ref.reflectString(in, outVal)
	case reflect.Interface:
		err = ref.reflectInterface(in, outVal)
	case reflect.Array:
		err = ref.reflectArray(fieldname, in, outVal)
	case reflect.Slice:
		err = ref.reflectSlice(fieldname, in, outVal)
	case reflect.Map:
		err = ref.reflectMap(fieldname, in, outVal)
	case reflect.Struct:
		err = ref.reflectStruct(fieldname, in, outVal)

	default:
		return fmt.Errorf("type %s not supported", outValKind)
	}

	return err
}

func (ref *marshaler) reflectBool(in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))
	inKind := getNumKind(inVal)

	switch {

	case inKind == reflect.Int:
		outVal.SetBool(inVal.Int() != 0)

	case inKind == reflect.Uint:
		outVal.SetBool(inVal.Uint() != 0)

	case inKind == reflect.Float32:
		outVal.SetBool(inVal.Float() != 0)

	case inKind == reflect.Bool:
		outVal.SetBool(inVal.Bool())

	case inKind == reflect.String:
		t, err := strconv.ParseBool(inVal.String())
		if err == nil {
			outVal.SetBool(t)

		} else if len(inVal.String()) == 0 {
			outVal.SetBool(false)

		} else {
			return fmt.Errorf("fail to parse boolean: %s", err)
		}

	default:
		return fmt.Errorf("fail to convert type '%s' to '%s', data: %v", inVal.Type(), outVal.Type(), in)
	}

	return nil
}

func (ref *marshaler) reflectInt(in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))
	inKind := getNumKind(inVal)

	switch {
	case inKind == reflect.Int:
		outVal.SetInt(inVal.Int())

	case inKind == reflect.Uint:
		outVal.SetInt(int64(inVal.Uint()))

	case inKind == reflect.Float32:
		outVal.SetInt(int64(inVal.Float()))

	case inKind == reflect.Bool:
		if inVal.Bool() {
			outVal.SetInt(1)
		} else {
			outVal.SetInt(0)
		}

	case inKind == reflect.String:
		v := inVal.String()
		if len(v) == 0 {
			v = NIL
		}

		intStr, err := strconv.ParseInt(v, 0, outVal.Type().Bits())
		if err == nil {
			outVal.SetInt(intStr)
		} else {
			return fmt.Errorf("fail to parse int: %s", err)
		}

	default:
		return fmt.Errorf("fail to convert type '%s' to '%s', data: %v", inVal.Type(), outVal.Type(), in)
	}

	return nil
}

func (ref *marshaler) reflectUint(in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))
	inKind := getNumKind(inVal)

	switch {
	case inKind == reflect.Int:
		intNum := inVal.Int()
		if intNum < 0 {
			return fmt.Errorf("fail to parse %d (uint overflow)", intNum)
		}
		outVal.SetUint(uint64(intNum))

	case inKind == reflect.Uint:
		outVal.SetUint(inVal.Uint())

	case inKind == reflect.Float32:
		floadNum := inVal.Float()
		if floadNum < 0 {
			return fmt.Errorf("fail to parse %f (uint overflow)", floadNum)
		}
		outVal.SetUint(uint64(floadNum))

	case inKind == reflect.Bool:
		if inVal.Bool() {
			outVal.SetUint(1)
		} else {
			outVal.SetUint(0)
		}

	case inKind == reflect.String:
		v := inVal.String()
		if len(v) == 0 {
			v = NIL
		}

		uintNum, err := strconv.ParseUint(v, 0, outVal.Type().Bits())
		if err == nil {
			outVal.SetUint(uintNum)
		} else {
			return fmt.Errorf("fail to parse uint: %s", err)
		}

	default:
		return fmt.Errorf("fail to convert type '%s' to '%s', data: %v", inVal.Type(), outVal.Type(), in)
	}

	return nil
}

func (ref *marshaler) reflectFloat(in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))
	inKind := getNumKind(inVal)

	switch {
	case inKind == reflect.Int:
		outVal.SetFloat(float64(inVal.Int()))

	case inKind == reflect.Uint:
		outVal.SetFloat(float64(inVal.Uint()))

	case inKind == reflect.Float32:
		outVal.SetFloat(inVal.Float())

	case inKind == reflect.Bool:
		if inVal.Bool() {
			outVal.SetFloat(1)
		} else {
			outVal.SetFloat(0)
		}

	case inKind == reflect.String:
		v := inVal.String()
		if len(v) == 0 {
			v = NIL
		}

		f, err := strconv.ParseFloat(v, outVal.Type().Bits())
		if err == nil {
			outVal.SetFloat(f)
		} else {
			return fmt.Errorf("fail to parse float: %s", err)
		}

	default:
		return fmt.Errorf("fail to convert type '%s' to '%s', data: %v", inVal.Type(), outVal.Type(), in)
	}

	return nil
}

func (ref *marshaler) reflectString(in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))
	dataKind := getNumKind(inVal)

	switch {
	case dataKind == reflect.Int:
		outVal.SetString(strconv.FormatInt(inVal.Int(), 10))

	case dataKind == reflect.Uint:
		outVal.SetString(strconv.FormatUint(inVal.Uint(), 10))

	case dataKind == reflect.Float32:
		outVal.SetString(strconv.FormatFloat(inVal.Float(), 'f', -1, 64))

	case dataKind == reflect.Bool:
		if inVal.Bool() {
			outVal.SetString(ONE)
		} else {
			outVal.SetString(NIL)
		}

	case dataKind == reflect.String:
		outVal.SetString(inVal.String())

	default:
		return fmt.Errorf("fail to convert type '%s' to '%s', data: %v", inVal.Type(), outVal.Type(), in)
	}

	return nil
}

func (ref *marshaler) reflectInterface(in interface{}, outVal reflect.Value) error {
	if outVal.IsValid() && outVal.Elem().IsValid() {
		elem := outVal.Elem()

		copied := false
		if !elem.CanAddr() {
			copied = true
			copy := reflect.New(elem.Type())
			copy.Elem().Set(elem)
			elem = copy
		}

		if err := ref.marshal(EMPTY, in, elem); err != nil || !copied {
			return err
		}

		outVal.Set(elem.Elem())
		return nil
	}

	inVal := reflect.ValueOf(in)

	if inVal.Kind() == reflect.Ptr && inVal.Type().Elem() == outVal.Type() {
		inVal = reflect.Indirect(inVal)
	}

	if !inVal.IsValid() {
		inVal = reflect.Zero(outVal.Type())
	}

	inValType := inVal.Type()
	if !inValType.AssignableTo(outVal.Type()) {
		return fmt.Errorf("fail to convert type %s, got %s", outVal.Type(), inValType)
	}

	outVal.Set(inVal)
	return nil
}

func (ref *marshaler) map2struct(fieldName string, inVal, val reflect.Value) error {
	inValType := inVal.Type()
	if kind := inValType.Key().Kind(); kind != reflect.String && kind != reflect.Interface {
		return fmt.Errorf("field %s needs a map with keys, got '%s' keys", fieldName, inValType.Key().Kind())
	}

	inValKeys := make(map[reflect.Value]struct{})
	inValKeysUnuse := make(map[interface{}]struct{})
	for _, inValKey := range inVal.MapKeys() {
		inValKeys[inValKey] = struct{}{}
		inValKeysUnuse[inValKey.Interface()] = struct{}{}
	}

	structs := make([]reflect.Value, 1, 5)
	structs[0] = val

	fields := []inputstructfield{}
	for len(structs) > 0 {
		structVal := structs[0]
		structs = structs[1:]

		structType := structVal.Type()

		for i := 0; i < structType.NumField(); i++ {
			fieldType := structType.Field(i)
			fieldVal := structVal.Field(i)
			if fieldVal.Kind() == reflect.Ptr && fieldVal.Elem().Kind() == reflect.Struct {
				fieldVal = fieldVal.Elem()
			}

			fields = append(fields, inputstructfield{fieldType, fieldVal})
		}
	}

	errStack := make([]string, 0)
	for _, f := range fields {
		field, fieldValue := f.field, f.val
		fN := field.Name

		tagValue := field.Tag.Get(TAG)
		if tagValue != EMPTY {
			fN = tagValue
		}

		rawMapKey := reflect.ValueOf(fN)
		rawMapVal := inVal.MapIndex(rawMapKey)
		if !rawMapVal.IsValid() {
			for inValKey := range inValKeys {
				mK, ok := inValKey.Interface().(string)
				if !ok {
					continue
				}

				if ref.output.mappingName(mK, fN) {
					rawMapKey = inValKey
					rawMapVal = inVal.MapIndex(inValKey)
					break
				}
			}

			if !rawMapVal.IsValid() {
				continue
			}
		}

		if !fieldValue.IsValid() {
			panic("幹 不可能")
		}

		if !fieldValue.CanSet() {
			continue
		}

		delete(inValKeysUnuse, rawMapKey.Interface())

		if fieldName != EMPTY {
			fN = fieldName + PERIOD + fN
		}

		if err := ref.marshal(fN, rawMapVal.Interface(), fieldValue); err != nil {
			errStack = appendErrors(errStack, err)
		}
	}

	if len(errStack) > 0 {
		return &Error{errStack}
	}

	return nil
}

func (ref *marshaler) reflectStruct(fieldName string, in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))

	if inVal.Type() == outVal.Type() {
		outVal.Set(inVal)
		return nil
	}

	inValKind := inVal.Kind()
	switch inValKind {
	case reflect.Map:
		return ref.map2struct(fieldName, inVal, outVal)

	case reflect.Struct:
		mapType := reflect.TypeOf((map[string]interface{})(nil))
		mVal := reflect.MakeMap(mapType)
		addrVal := reflect.New(mVal.Type())

		reflect.Indirect(addrVal).Set(mVal)
		if err := ref.struct2map(inVal, reflect.Indirect(addrVal), mVal); err != nil {
			return err
		}

		return ref.map2struct(fieldName, reflect.Indirect(addrVal), outVal)

	default:
		return fmt.Errorf("fail to convert struct, got: %s", inVal.Kind())
	}
}

func (ref *marshaler) struct2map(inVal, outVal, mapVal reflect.Value) error {
	// valType := inVal.Type()

	// for i := 0; i < valType.NumField(); i++ {
	// 	vtf := valType.Field(i)
	// 	fmt.Println("X", vtf.PkgPath)
	// 	if vtf.PkgPath != EMPTY {
	// 		continue
	// 	}

	// 	inv := inVal.Field(i)
	// 	if !inv.Type().AssignableTo(mapVal.Type().Elem()) {
	// 		return fmt.Errorf("fail to assign type '%s' to map value field '%s'", inv.Type(), mapVal.Type().Elem())
	// 	}

	// 	tag := vtf.Tag.Get(TAG)
	// 	keyName := vtf.Name

	// 	if len(tag) > 0 {
	// 		keyName = tag
	// 	}

	// 	switch inv.Kind() {

	// 	case reflect.Struct:
	// 		rn := reflect.New(inv.Type())
	// 		rn.Elem().Set(inv)

	// 		vType := mapVal.Type()
	// 		vKeyType := vType.Key()
	// 		vElemType := vType.Elem()
	// 		mType := reflect.MapOf(vKeyType, vElemType)
	// 		vMap := reflect.MakeMap(mType)

	// 		addrVal := reflect.New(vMap.Type())
	// 		reflect.Indirect(addrVal).Set(vMap)

	// 		err := ref.marshal(keyName, rn.Interface(), reflect.Indirect(addrVal))
	// 		if err != nil {
	// 			return err
	// 		}

	// 		vMap = reflect.Indirect(addrVal)

	// 		mapVal.SetMapIndex(reflect.ValueOf(keyName), vMap)

	// 	default:
	// 		mapVal.SetMapIndex(reflect.ValueOf(keyName), inv)
	// 	}
	// }

	if outVal.CanAddr() {
		outVal.Set(mapVal)
	}

	return nil
}

func (ref *marshaler) reflectMap(fieldName string, in interface{}, outVal reflect.Value) error {
	valType := outVal.Type()
	valKeyType := valType.Key()
	valElemType := valType.Elem()

	valMap := outVal

	if valMap.IsNil() {
		mapType := reflect.MapOf(valKeyType, valElemType)
		valMap = reflect.MakeMap(mapType)
	}

	inVal := reflect.Indirect(reflect.ValueOf(in))
	switch inVal.Kind() {
	case reflect.Map:
		return ref.map2map(fieldName, inVal, outVal, valMap)

	case reflect.Struct:
		return ref.struct2map(inVal, outVal, valMap)

	case reflect.Array, reflect.Slice:
		return ref.slice2map(fieldName, inVal, outVal, valMap)

	default:
		return fmt.Errorf("fail to convert map, got: %s", inVal.Kind())
	}
}

func (ref *marshaler) slice2map(fieldName string, inVal, outVal, mapVal reflect.Value) error {
	if inVal.Len() == 0 {
		outVal.Set(mapVal)
		return nil
	}

	for i := 0; i < inVal.Len(); i++ {
		fN := fieldName + SQUARE_BRACKETS_L + strconv.Itoa(i) + SQUARE_BRACKETS_R

		err := ref.marshal(fN, inVal.Index(i).Interface(), outVal)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ref *marshaler) map2map(fieldName string, inVal, outVal, mapVal reflect.Value) error {
	valType := outVal.Type()
	valKeyType := valType.Key()
	valElemType := valType.Elem()

	if inVal.Len() == 0 {
		if inVal.IsNil() {
			if !outVal.IsNil() {
				outVal.Set(inVal)
			}

		} else {
			outVal.Set(mapVal)
		}

		return nil
	}

	errStack := make([]string, 0)

	for _, k := range inVal.MapKeys() {
		fN := fieldName + SQUARE_BRACKETS_L + k.String() + SQUARE_BRACKETS_R
		key := reflect.Indirect(reflect.New(valKeyType))
		if err := ref.marshal(fN, k.Interface(), key); err != nil {
			errStack = appendErrors(errStack, err)
			continue
		}

		vi := inVal.MapIndex(k).Interface()
		v := reflect.Indirect(reflect.New(valElemType))
		if err := ref.marshal(fN, vi, v); err != nil {
			errStack = appendErrors(errStack, err)
			continue
		}

		mapVal.SetMapIndex(key, v)
	}

	outVal.Set(mapVal)

	if len(errStack) > 0 {
		return &Error{errStack}
	}

	return nil
}

func (ref *marshaler) reflectArray(fieldName string, in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))
	inValKind := inVal.Kind()

	valType := outVal.Type()
	valElemType := valType.Elem()

	arrayType := reflect.ArrayOf(valType.Len(), valElemType)

	valArray := outVal

	if valArray.Interface() == reflect.Zero(valArray.Type()).Interface() {
		if inValKind != reflect.Array && inValKind != reflect.Slice {
			switch {
			case inValKind == reflect.Map:
				if inVal.Len() == 0 {
					outVal.Set(reflect.Zero(arrayType))
					return nil
				}

			default:
				return ref.reflectArray(fieldName, []interface{}{in}, outVal)
			}

			return fmt.Errorf("input data type(%s) should be an array or slice", inValKind)

		}
		if inVal.Len() > arrayType.Len() {
			return fmt.Errorf("input data '%s' length should be less or equal to %d, got %d", fieldName, arrayType.Len(), inVal.Len())

		}

		valArray = reflect.New(arrayType).Elem()
	}

	errStack := make([]string, 0)

	for i := 0; i < inVal.Len(); i++ {
		data := inVal.Index(i).Interface()
		field := valArray.Index(i)

		fN := fieldName + SQUARE_BRACKETS_L + strconv.Itoa(i) + SQUARE_BRACKETS_R
		if err := ref.marshal(fN, data, field); err != nil {
			errStack = appendErrors(errStack, err)
		}
	}

	outVal.Set(valArray)

	if len(errStack) > 0 {
		return &Error{errStack}
	}

	return nil
}

func (ref *marshaler) reflectSlice(fieldName string, in interface{}, outVal reflect.Value) error {
	inVal := reflect.Indirect(reflect.ValueOf(in))
	inValKind := inVal.Kind()

	valType := outVal.Type()
	valElemType := valType.Elem()

	sliceType := reflect.SliceOf(valElemType)

	if inValKind != reflect.Array && inValKind != reflect.Slice {
		switch {
		case inValKind == reflect.Slice, inValKind == reflect.Array:
			break

		case inValKind == reflect.Map:
			if inVal.Len() == 0 {
				outVal.Set(reflect.MakeSlice(sliceType, 0, 0))
				return nil
			}

			return ref.reflectSlice(fieldName, []interface{}{in}, outVal)

		case inValKind == reflect.String && valElemType.Kind() == reflect.Uint8:
			return ref.reflectSlice(fieldName, []byte(inVal.String()), outVal)

		default:

			return ref.reflectSlice(fieldName, []interface{}{in}, outVal)
		}

		return fmt.Errorf("input data type(%s) should be an array or slice", inValKind)
	}

	if inVal.IsNil() {
		return nil
	}

	valSlice := outVal
	if valSlice.IsNil() {
		valSlice = reflect.MakeSlice(sliceType, inVal.Len(), inVal.Len())
	}

	errStack := make([]string, 0)

	for i := 0; i < inVal.Len(); i++ {
		data := inVal.Index(i).Interface()
		for valSlice.Len() <= i {
			valSlice = reflect.Append(valSlice, reflect.Zero(valElemType))
		}
		field := valSlice.Index(i)

		fN := fieldName + SQUARE_BRACKETS_L + strconv.Itoa(i) + SQUARE_BRACKETS_R
		if err := ref.marshal(fN, data, field); err != nil {
			errStack = appendErrors(errStack, err)
		}
	}

	outVal.Set(valSlice)

	if len(errStack) > 0 {
		return &Error{errStack}
	}

	return nil
}
