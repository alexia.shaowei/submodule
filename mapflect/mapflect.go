package mapflect

import "reflect"

type marshalerOut struct {
	out         interface{}
	mappingName func(mapKey, fieldName string) bool
}

type marshaler struct {
	output *marshalerOut
}

type inputstructfield struct {
	field reflect.StructField
	val   reflect.Value
}

type Error struct {
	Errors []string
}

func Marshal(in, out interface{}) error {
	opt := &marshalerOut{
		out: out,
	}

	marshaler, err := newMarshaler(opt)
	if err != nil {
		return err
	}

	return marshaler.Marshal(in)
}
