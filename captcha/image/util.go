package ftimage

func minXYZ(x, y, z uint8) (min uint8) {
	min = x
	if y < min {
		min = y
	}
	if z < min {
		min = z
	}

	return
}

func maxXYZ(x, y, z uint8) (max uint8) {
	max = x
	if y > max {
		max = y
	}
	if z > max {
		max = z
	}

	return
}
