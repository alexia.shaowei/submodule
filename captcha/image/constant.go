package ftimage

const (
	DefaultWidth  = 240
	DefaultHeight = 80
	DefaultSkew   = 1
	DefaultDotCnt = 20
)

const (
	fontWidth    = 11
	fontHeight   = 18
	focusElement = 1
)
