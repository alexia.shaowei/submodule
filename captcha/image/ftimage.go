package ftimage

import (
	"bytes"
	"encoding/base64"
	"image"
	"image/color"
	"image/png"
	"io"
	"math"

	random "gitlab.com/alexia.shaowei/submodule/captcha"
)

func New(id string, digits []byte, width, height int) *Image {
	img := new(Image)

	img.prng.Seed(random.PRNGSeed(0x01, id, digits))
	img.Paletted = image.NewPaletted(image.Rect(0, 0, width, height), img.randomPalette())
	img.determineSize(width, height, len(digits))

	xLimit := width - (img.imgWidth+img.dotSize)*len(digits) - img.dotSize
	yLimit := height - img.imgHeigth - img.dotSize*2
	var border int
	if width > height {
		border = height / 5
	} else {
		border = width / 5
	}

	x := img.prng.Int(border, xLimit-border)
	y := img.prng.Int(border, yLimit-border)

	for _, n := range digits {
		img.draw(models[n], x, y)
		x += img.imgWidth + img.dotSize
	}

	img.strikeThrough()
	img.distort(img.prng.Float(5, 10), img.prng.Float(100, 200))
	img.fillWithCircles(DefaultDotCnt, img.dotSize)

	return img

}

type Image struct {
	*image.Paletted
	imgWidth  int
	imgHeigth int
	dotSize   int
	prng      PRNG
}

func (ref *Image) randomPalette() color.Palette {
	prims := make([]color.Color, DefaultDotCnt+1)

	prim := color.RGBA{
		uint8(ref.prng.Intn(129)),
		uint8(ref.prng.Intn(129)),
		uint8(ref.prng.Intn(129)),
		0xFF,
	}

	prims[0] = color.RGBA{0xFF, 0xFF, 0xFF, 0x00}
	prims[1] = prim

	for i := 2; i <= DefaultDotCnt; i++ {
		prims[i] = ref.randomBrightness(prim, 255)
	}

	return prims
}

func (ref *Image) randomBrightness(c color.RGBA, max uint8) color.RGBA {
	minVal := minXYZ(c.R, c.G, c.B)
	maxVal := maxXYZ(c.R, c.G, c.B)
	if maxVal > max {
		return c
	}

	n := ref.prng.Intn(int(max-maxVal)) - int(minVal)

	return color.RGBA{
		uint8(int(c.R) + n),
		uint8(int(c.G) + n),
		uint8(int(c.B) + n),
		uint8(c.A),
	}
}

func (ref *Image) determineSize(width, height, ncount int) {

	var border int
	if width > height {
		border = height / 4
	} else {
		border = width / 4
	}

	w := float64(width - border*2)
	h := float64(height - border*2)

	fw := float64(fontWidth + 1)
	fh := float64(fontHeight)
	nc := float64(ncount)

	nw := w / nc

	nh := nw * fh / fw
	if nh > h {
		nh = h
		nw = fw / fh * nh
	}

	ref.dotSize = int(nh / fh)
	if ref.dotSize < 1 {
		ref.dotSize = 1
	}

	ref.imgWidth = int(nw) - ref.dotSize
	ref.imgHeigth = int(nh)
}

func (ref *Image) draw(digit []byte, x, y int) {
	skf := ref.prng.Float(-DefaultSkew, DefaultSkew)
	xs := float64(x)
	r := ref.dotSize / 2
	y += ref.prng.Int(-r, r)
	for yo := 0; yo < fontHeight; yo++ {
		for xo := 0; xo < fontWidth; xo++ {
			if digit[yo*fontWidth+xo] != focusElement {
				continue
			}

			ref.drawCircle(x+xo*ref.dotSize, y+yo*ref.dotSize, r, 1)
		}

		xs += skf
		x = int(xs)
	}
}

func (ref *Image) drawCircle(x, y, radius int, colorIdx uint8) {
	f := 1 - radius
	dfx := 1
	dfy := -2 * radius
	xo := 0
	yo := radius

	ref.SetColorIndex(x, y+radius, colorIdx)
	ref.SetColorIndex(x, y-radius, colorIdx)
	ref.drawHorizLine(x-radius, x+radius, y, colorIdx)

	for xo < yo {
		if f >= 0 {
			yo--
			dfy += 2
			f += dfy
		}

		xo++
		dfx += 2
		f += dfx
		ref.drawHorizLine(x-xo, x+xo, y+yo, colorIdx)
		ref.drawHorizLine(x-xo, x+xo, y-yo, colorIdx)
		ref.drawHorizLine(x-yo, x+yo, y+xo, colorIdx)
		ref.drawHorizLine(x-yo, x+yo, y-xo, colorIdx)
	}
}

func (m *Image) drawHorizLine(fromX, toX, y int, colorIdx uint8) {
	for x := fromX; x <= toX; x++ {
		m.SetColorIndex(x, y, colorIdx)
	}
}

func (ref *Image) strikeThrough() {
	maxx := ref.Bounds().Max.X
	maxy := ref.Bounds().Max.Y
	y := ref.prng.Int(maxy/3, maxy-maxy/3)
	amplitude := ref.prng.Float(5, 20)
	period := ref.prng.Float(80, 180)
	dx := 2.0 * math.Pi / period
	for x := 0; x < maxx; x++ {
		xo := amplitude * math.Cos(float64(y)*dx)
		yo := amplitude * math.Sin(float64(x)*dx)
		for yn := 0; yn < ref.dotSize; yn++ {
			r := ref.prng.Int(0, ref.dotSize)
			ref.drawCircle(x+int(xo), y+int(yo)+(yn*ref.dotSize), r/2, 1)
		}
	}
}

func (ref *Image) distort(amplude float64, period float64) {
	w := ref.Bounds().Max.X
	h := ref.Bounds().Max.Y

	oldm := ref.Paletted
	newm := image.NewPaletted(image.Rect(0, 0, w, h), oldm.Palette)

	dx := 2.0 * math.Pi / period
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			xo := amplude * math.Sin(float64(y)*dx)
			yo := amplude * math.Cos(float64(x)*dx)
			newm.SetColorIndex(x, y, oldm.ColorIndexAt(x+int(xo), y+int(yo)))
		}
	}
	ref.Paletted = newm
}

func (ref *Image) fillWithCircles(n, maxradius int) {
	maxX := ref.Bounds().Max.X
	maxY := ref.Bounds().Max.Y
	for i := 0; i < n; i++ {
		colorIdx := uint8(ref.prng.Int(1, DefaultDotCnt-1))
		r := ref.prng.Int(1, maxradius)
		ref.drawCircle(ref.prng.Int(r, maxX-r), ref.prng.Int(r, maxY-r), r, colorIdx)
	}
}

func (ref *Image) png() []byte {
	var buf bytes.Buffer
	if err := png.Encode(&buf, ref.Paletted); err != nil {
		panic(err.Error())
	}
	return buf.Bytes()
}

func (ref *Image) Out(w io.Writer) (int64, error) {
	cnt, err := w.Write(ref.png())

	return int64(cnt), err
}

func (ref *Image) Base64PNG(w io.Writer) (int64, error) {
	cnt, err := w.Write([]byte(base64.StdEncoding.EncodeToString(ref.png())))

	return int64(cnt), err
}
