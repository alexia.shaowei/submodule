package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	captcha "gitlab.com/alexia.shaowei/submodule/captcha"
	img "gitlab.com/alexia.shaowei/submodule/captcha/image"
)

type ResponseData struct {
	Success bool        `json:"success"`
	ErrCode int         `json:"errCode"`
	ErrMsg  string      `json:"errMsg"`
	Data    interface{} `json:"data"`
}

func resp(data interface{}) []byte {

	r := ResponseData{
		Success: true,
		ErrCode: -1,
		ErrMsg:  "",
		Data:    data,
	}

	j, _ := json.Marshal(&r)

	return j
}

func main() {
	fmt.Println("captcha::main()")

	http.HandleFunc("/img/1", imageH)
	http.HandleFunc("/img/2", imageH2)
	http.HandleFunc("/base64", imageHandler)
	if err := http.ListenAndServe("localhost:8800", nil); err != nil {
		log.Fatal(err)
	}

}

func imageH(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/png")

	var bbt bytes.Buffer
	img.New("1", []byte{5, 5, 7, 8, 4}, 240, 80).Out(&bbt)

	w.Write(bbt.Bytes())

}

func imageH2(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/png")

	var bbt bytes.Buffer
	img.New("sfwef151", []byte{5, 5, 7, 8, 4}, 240, 80).Out(&bbt)

	w.Write(bbt.Bytes())

}

func imageHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	var bbt bytes.Buffer
	img.New("1", []byte{5, 5, 7, 8, 4}, 240, 80).Base64PNG(&bbt)

	a := struct {
		Success   bool
		ErrorCode int
		ErrorMsg  string
		Data      string
	}{
		Success:   true,
		ErrorCode: -1,
		ErrorMsg:  "",
		Data:      bbt.String(),
	}

	b, _ := json.Marshal(&a)

	w.Write(b)

}

func cpt(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	kV := captcha.New(4)

	w.Write(resp(kV))
}

func cptls(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	w.Write(resp(captcha.All()))

}
