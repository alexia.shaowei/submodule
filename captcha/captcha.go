package ftcaptcha

import "bytes"

func New(length int) string {
	uid := randomId()
	num := randomDigits(length)

	storage.Put(uid, num)

	return uid
}

func randomId() string {
	b := randomBytesMod(idLen, byte(len(idChars)))
	for i, c := range b {
		b[i] = idChars[c]
	}

	return string(b)
}

func Reload(uid string) bool {
	org := storage.Get(uid, false)
	if org == nil {
		return false
	}

	storage.Put(uid, randomDigits(len(org)))

	return true
}

func Verify(id string, digits []byte) bool {
	if digits == nil {
		return false
	}

	if len(digits) == 0 {
		return false
	}

	reald := storage.Get(id, true)
	if reald == nil {
		return false
	}

	return bytes.Equal(digits, reald)
}

func VerifyString(id string, digits string) bool {
	if digits == "" {
		return false
	}

	ns := make([]byte, len(digits))
	for i := range ns {
		d := digits[i]
		switch {
		case '0' <= d && d <= '9':
			ns[i] = d - '0'

		case d == ' ' || d == ',':

		default:
			return false
		}
	}
	return Verify(id, ns)
}

func All() string {
	return storage.All()
}
