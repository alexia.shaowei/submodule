package ftcaptcha

import (
	"container/list"
	"fmt"
	"sync"
	"time"
)

type Storage interface {
	Put(uid string, digits []byte)
	Get(uid string, kill bool) (digits []byte)
	All() string
}

type idByTimeVal struct {
	timestamp time.Time
	uid       string
}

type memoryCache struct {
	sync.RWMutex
	digitsByID map[string][]byte
	idByTime   *list.List
	numStored  int
	gcNum      int
	expiration time.Duration
}

func newMemoryCache(gcNum int, expiration time.Duration) Storage {
	mc := new(memoryCache)
	mc.digitsByID = make(map[string][]byte)
	mc.idByTime = list.New()
	mc.gcNum = gcNum
	mc.expiration = expiration

	return mc
}

func (ref *memoryCache) Put(id string, digits []byte) {
	ref.Lock()
	ref.digitsByID[id] = digits
	ref.idByTime.PushBack(idByTimeVal{time.Now(), id})
	ref.numStored++

	if ref.numStored <= ref.gcNum {
		ref.Unlock()
		return
	}

	ref.Unlock()

	go ref.gc()
}

func (ref *memoryCache) Get(id string, kill bool) (digits []byte) {
	if !kill {
		ref.RLock()
		defer ref.RUnlock()

	} else {
		ref.Lock()
		defer ref.Unlock()
	}

	digits, ok := ref.digitsByID[id]
	if !ok {
		return
	}

	if kill {
		delete(ref.digitsByID, id)
	}

	return
}

func (ref *memoryCache) gc() {
	fmt.Println("gc init")
	now := time.Now()
	ref.Lock()
	defer ref.Unlock()

	ref.numStored = 0
	for e := ref.idByTime.Front(); e != nil; {
		fmt.Println("gc")

		ev, ok := e.Value.(idByTimeVal)
		if !ok {
			return
		}

		if ev.timestamp.Add(ref.expiration).Before(now) {
			delete(ref.digitsByID, ev.uid)

			next := e.Next()
			ref.idByTime.Remove(e)

			e = next

		} else {
			return
		}
	}

	fmt.Println("down")
}

func (ref *memoryCache) All() string {
	v1 := fmt.Sprintf("%+v\n", ref.digitsByID)
	v2 := fmt.Sprintf("%+v\n", ref.idByTime)

	return fmt.Sprintf("v1: %v\nv2: %v", v1, v2)
}
