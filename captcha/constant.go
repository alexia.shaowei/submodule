package ftcaptcha

import "time"

const (
	DefaultLen = 4
	GCNum      = 100
	Expiration = time.Minute * 10
)

var storage = newMemoryCache(GCNum, Expiration)
