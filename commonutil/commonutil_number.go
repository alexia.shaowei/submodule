package commonutil

import (
	"strconv"
	"strings"
)

func StrToInt(strNum string) int {
	num, err := strconv.Atoi(strings.Trim(strNum, " "))
	if err != nil {
		return 0
	}
	return num
}

func StrToInt8(strNum string) int8 {
	num, err := strconv.ParseInt(strings.Trim(strNum, " "), 10, 8)
	if err != nil {
		return int8(0)
	}
	return int8(num)
}

func StrToInt16(strNum string) int16 {
	num, err := strconv.ParseInt(strings.Trim(strNum, " "), 10, 16)
	if err != nil {
		return int16(0)
	}
	return int16(num)
}

func StrToInt32(strNum string) int32 {
	num, err := strconv.ParseInt(strings.Trim(strNum, " "), 10, 32)
	if err != nil {
		return int32(0)
	}
	return int32(num)
}

func StrToUint8(strNum string) uint8 {
	num, err := strconv.ParseUint(strings.Trim(strNum, " "), 10, 8)
	if err != nil {
		return uint8(0)
	}
	return uint8(num)
}

func StrToUint16(strNum string) uint16 {
	num, err := strconv.ParseUint(strings.Trim(strNum, " "), 10, 16)
	if err != nil {
		return uint16(0)
	}
	return uint16(num)
}

func StrToUint32(strNum string) uint32 {
	num, err := strconv.ParseUint(strings.Trim(strNum, " "), 10, 32)
	if err != nil {
		return uint32(0)
	}
	return uint32(num)
}

func StrToUint64(strNum string) uint64 {
	num, err := strconv.ParseUint(strings.Trim(strNum, " "), 10, 64)
	if err != nil {
		return uint64(0)
	}
	return uint64(num)
}

func StrToFloat32(strNum string) float32 {
	num, err := strconv.ParseFloat(strings.Trim(strNum, " "), 32)
	if err != nil {
		return float32(0)
	}
	return float32(num)
}

func StrToFloat64(strNum string) float64 {
	num, err := strconv.ParseFloat(strings.Trim(strNum, " "), 64)
	if err != nil {
		return float64(0)
	}
	return float64(num)
}

func IsNumberic(num string) bool {
	_, err := strconv.ParseFloat(num, 64)
	return err == nil
}
