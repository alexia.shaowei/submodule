package commonutil

import (
	"encoding/base64"
	"fmt"
	"os"
	"strings"
	"sync"
)

func MakeNestFolder(path string) error {

	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}

func IsExistFolder(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}

	return true
}

func Base64ImageDecode(base64Str string, path string) (string, error) {

	base64Str = strings.Trim(base64Str, " ")

	extension := ""
	subLen := 0

	if strings.HasPrefix(base64Str, BASE64_PREFIX_JPEG) {
		extension = "jpeg"
		subLen = len(BASE64_PREFIX_JPEG)
	} else if strings.HasPrefix(base64Str, BASE64_PREFIX_PNG) {
		extension = "gif"
		subLen = len(BASE64_PREFIX_PNG)
	} else if strings.HasPrefix(base64Str, BASE64_PREFIX_GIF) {
		extension = "png"
		subLen = len(BASE64_PREFIX_GIF)
	} else {
		return "", fmt.Errorf("base64 decode error, no supported type")
	}

	base64Str = base64Str[subLen:]

	byteFile, err := base64.StdEncoding.DecodeString(base64Str)
	if err != nil {
		return "", err
	}

	imageName := fmt.Sprintf("%s.%s", MD5(base64Str), extension)

	file, err := os.Create(fmt.Sprintf("%s/%s", path, imageName))
	if err != nil {
		return "", err
	}

	file.Write(byteFile)

	file.Close()

	return imageName, nil
}

func Base64MultiImagesDecode(base64Slice []string, path string) []string {

	// var rtnImageNameList []string = make([]string, 0)

	// ch := make(chan struct{}, len(base64Slice))
	// for _, base64Str := range base64Slice {
	// 	go func(base64Str string) {
	// 		imgName, err := Base64ImageDecode(base64Str, path)
	// 		if err == nil {
	// 			rtnImageNameList = append(rtnImageNameList, imgName)
	// 		}

	// 		ch <- struct{}{}

	// 	}(base64Str)
	// }

	// for {

	// }

	var rtnImageNameList []string = make([]string, 0)

	var wg sync.WaitGroup

	wg.Add(len(base64Slice))

	for i := 0; i < len(base64Slice); i++ {
		go func(base64Str string) {
			imgName, err := Base64ImageDecode(base64Str, path)
			if err == nil {
				rtnImageNameList = append(rtnImageNameList, imgName)
			}

			wg.Done()

		}(base64Slice[i])
	}

	wg.Wait()

	return rtnImageNameList
}
