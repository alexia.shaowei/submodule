package commonutil

const (
	ERROR_CODE_NONE                                 = -1
	ERROR_CODE_FILE_UPLOAD                          = 10
	ERROR_CODE_IMAGE_UPLOAD                         = 11
	ERROR_CODE_BASE64_IMAGE_UPLOAD                  = 12
	ERROR_CODE_ACCESS_DENIED                        = 500
	ERROR_CODE_DATE_TO_JSON                         = 10000
	ERROR_CODE_JSON_TO_DATA                         = 10001
	ERROR_CODE_DB_SYSTEM                            = 20000
	ERROR_CODE_DB_SYNTAX                            = 20001
	ERROR_CODE_DB_NOUPDATE                          = 20002
	ERROR_CODE_RARAMETER_EMPTY                      = 40000
	ERROR_CODE_ACCOUNT_EXIST                        = 40001
	ERROR_CODE_ACCOUNT_NOT_EXIST                    = 40002
	ERROR_CODE_ACCOUNT_ERR_PASSWORD                 = 40003
	ERROR_CODE_ACCOUNT_NOT_ACTIVATED                = 40004
	ERROR_CODE_ACCOUNT_LOGIN_PARAMETER              = 40005
	ERROR_CODE_ACCOUNT_LOGIN_NX_FREQUENCY           = 40006
	ERROR_CODE_ACCOUNT_LOGIN_ACCOUNT_LOCK           = 40007
	ERROR_CODE_ACCOUNT_REGIST_USERNAME_EMPTY        = 40101
	ERROR_CODE_ACCOUNT_REGIST_PASSWORD_EMPTY        = 40102
	ERROR_CODE_ACCOUNT_REGIST_PASSWORD_COMPARE      = 40103
	ERROR_CODE_ACCOUNT_REGIST_CONFIRMPASSWORD_EMPTY = 40104
	ERROR_CODE_ACCOUNT_REGIST_ACCOUNT_EXIST         = 40105
	ERROR_CODE_UNDEFINED                            = 99999
)

const (
	ERROR_CODE_MSG_NONE                                 = ""
	ERROR_CODE_MSG_FILE_UPLOAD                          = "file upload failed"
	ERROR_CODE_MSG_IMAGE_UPLOAD                         = "image upload failed"
	ERROR_CODE_MSG_BASE64_IMAGE_UPLOAD                  = "base64 image upload faild"
	ERROR_CODE_MSG_ACCESS_DENIED                        = "access denied"
	ERROR_CODE_MSG_DATA_TO_JSON                         = "transfer JSON failed"
	ERROR_CODE_MSG_JSON_TO_DATA                         = "transfer JSON failed"
	ERROR_CODE_MSG_DB_SYSTEM                            = "db failed"
	ERROR_CODE_MSG_DB_SYNTAX                            = "db failed"
	ERROR_CODE_MSG_DB_NOUPDATE                          = "no row update"
	ERROR_CODE_MSG_RARAMETER_EMPTY                      = "request parameter empty"
	ERROR_CODE_MSG_ACCOUNT_EXIST                        = "the account is already existed"
	ERROR_CODE_MSG_ACCOUNT_NOT_EXIST                    = "account not exist"
	ERROR_CODE_MSG_ACCOUNT_ERR_PASSWORD                 = "password incorrect"
	ERROR_CODE_MSG_ACCOUNT_NOT_ACTIVATED                = "account is not activated yet"
	ERROR_CODE_MSG_ACCOUNT_LOGIN_PARAMETER              = "login parameter error"
	ERROR_CODE_MSG_ACCOUNT_LOGIN_NX_FREQUENCY           = "maximum operating frequency"
	ERROR_CODE_MSG_ACCOUNT_LOGIN_ACCOUNT_LOCK           = "account locked"
	ERROR_CODE_MSG_ACCOUNT_REGIST_USERNAME_EMPTY        = "empty username"
	ERROR_CODE_MSG_ACCOUNT_REGIST_PASSWORD_EMPTY        = "empty password"
	ERROR_CODE_MSG_ACCOUNT_REGIST_PASSWORD_COMPARE      = "password and confirm password does not match"
	ERROR_CODE_MSG_ACCOUNT_REGIST_CONFIRMPASSWORD_EMPTY = "empty confirm password"
	ERROR_CODE_MSG_UNDEFINED                            = "undifined system error"
)
