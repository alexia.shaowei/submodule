package commonutil

const (
	ACCOUNT_LOGIN_REDIS_KEY          = "ACCOUNT_LOGIN_REDIS_KEY_%s"              // _username (don't use sha256 encryption)
	ACCOUNT_LOGIN_REDIS_NXKEY        = "ACCOUNT_LOGIN_REDIS_NX_KEY_%s"           // _username (don't use sha256 encryption)
	ACCOUNT_LOGIN_REDIS_SUCCESS_KEY  = "ACCOUNT_LOGIN_REDIS_SUCCESS_KEY_%s"      // _username (don't use sha256 encryption)
	ACCOUNT_LOGIN_PASSWORD_SHA256    = "ACCOUNT_PASSWORD_SHA256_%s::%s"          // _username::password for sha256 (ftcommonutil.SHA256)
	ACCOUNT_LOGIN_TOKEN_SHA256       = "ACCOUNT_LOGIN_TOKEN_%s::%s::%s::%d::%v"  // _username::password::randSequence::unixTime::uid (ftcommonutil.SHA256)
	ACCOUNT_LOGIN_SESSION_KEY_SHA256 = "ACCOUNT_LOGIN_SESSIONKEY_%d::%s::%d::%d" // _uid::username::unixtime::randInd (ftcommonutil.SHA256)
)
