package commonutil

import (
	"math/rand"
	"time"
)

func RandInt(min int, max int) int {

	rand.Seed(time.Now().UnixNano())

	return rand.Intn(max-min) + min
}

func RandSequence(length int) string {
	var random *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

	byt := make([]byte, length)
	for idx := range byt {
		byt[idx] = RAND_CHARSET[random.Intn(len(RAND_CHARSET))]
	}

	return string(byt)
}
