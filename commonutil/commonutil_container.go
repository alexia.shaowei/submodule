package commonutil

func SliceContainsStr(strSlice []string, strValue string) (int, bool) {
	for i, val := range strSlice {
		if val == strValue {
			return i, true
		}
	}

	return -1, false
}

func SearchIntSlice(container []int, head, tail, goal int) int {

	if head > tail {
		return -1
	}

	idx := (head + tail) / 2

	if goal == container[idx] {
		return idx
	} else if goal > container[idx] {
		return SearchIntSlice(container, idx+1, tail, goal)
	} else {
		return SearchIntSlice(container, head, idx-1, goal)
	}
}
