package commonutil

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"strings"
	"time"

	"github.com/gofrs/uuid"
)

func MD5(str string) string {

	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}

func UUIDv4() string {
	uid, err := uuid.NewV4()
	if err != nil {
		return ""
	}

	return uid.String()
}

func Base64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

func Base64Decode(str string) ([]byte, error) {
	result, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return []byte{}, err
	}
	return result, nil
}

func SHA256(key string) string {
	sha := sha256.New()
	sha.Write([]byte(key))

	return strings.ToUpper(fmt.Sprintf("%x", sha.Sum(nil)))
}

func SHA256AccountPassword(account string, password string) string {
	sha256Key := fmt.Sprintf(ACCOUNT_LOGIN_PASSWORD_SHA256, account, password)

	return SHA256(sha256Key)
}

func SHA256AccountToken(uid int, account string, password string) string {
	unixTime := uint32(time.Now().Unix())

	sha256Key := fmt.Sprintf(ACCOUNT_LOGIN_TOKEN_SHA256, account, password, RandSequence(8), unixTime, uid)

	return SHA256(sha256Key)
}

func SHA256AccountSession(uid int, account string) string {
	unixTime := uint32(time.Now().Unix())

	sha256Key := fmt.Sprintf(ACCOUNT_LOGIN_SESSION_KEY_SHA256, uid, account, RandInt(0, 999), unixTime)

	return SHA256(sha256Key)
}
