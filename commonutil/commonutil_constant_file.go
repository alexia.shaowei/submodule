package commonutil

const (
	BASE64_PREFIX_JPEG = "data:image/jpeg;base64,"
	BASE64_PREFIX_PNG  = "data:image/gif;base64,"
	BASE64_PREFIX_GIF  = "data:image/png;base64,"
)
