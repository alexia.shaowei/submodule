package commonutil

const (
	TIME_FORMAT_FULL_1 = "2006-01-02 15:04:05"
	TIME_FORMAT_FULL_2 = "2006/01/02 15:04:05"
	TIME_FORMAT_FULL_3 = "20060102-150405"
	TIME_FORMAT_FULL_4 = "20060102150405"

	TIME_FORMAT_DATE_1 = "2006-01-02"
	TIME_FORMAT_DATE_2 = "2006/01/02"
	TIME_FORMAT_DATE_3 = "200601021504"
	TIME_FORMAT_DATE_4 = "20060102"

	TIME_FORMAT_TIME_1 = "15:04:05"
	TIME_FORMAT_TIME_2 = "150405"
)

const (
	ZONE_UTC                = "UTC"
	ZONE_ASIA_TAIPEI        = "Asia/Taipei"
	ZONE_ASIA_SHANGHAI      = "Asia/Shanghai"
	ZONE_ASIA_TOKYO         = "Asia/Tokyo"
	ZONE_ASIA_SINGAPORE     = "Asia/Singapore"
	ZONE_ASIA_BANGKOK       = "Asia/Bangkok"
	ZONE_EUROPE_BERLIN      = "Europe/Berlin"
	ZONE_EUROPE_PARIS       = "Europe/Paris"
	ZONE_EUROPE_LONDON      = "Europe/London"
	ZONE_EUROPE_MOSCOW      = "Europe/Moscow"
	ZONE_AUSTRALIA_SYDNEY   = "Australia/Sydney"
	ZONE_AUSTRALIA_DARWIN   = "Australia/Darwin"
	ZONE_AUSTRALIA_BRISBANE = "Australia/Brisbane"
	ZONE_AMERICA_NEWYORK    = "America/New_York"
	ZONE_AMERICA_LOSANGELES = "America/Los_Angeles"
)
