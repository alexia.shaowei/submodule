package commonutil

import (
	"time"
)

func TimeLocal(zone string) (uint32, string, error) {

	if len(zone) == 0 {
		zone = ZONE_ASIA_TAIPEI
	}

	_zone, err := time.LoadLocation(zone)

	if err != nil {
		return 0, "", err
	}

	utc := time.Now().UTC()
	newtime := utc.In(_zone)

	now := uint32(newtime.Unix())
	return now, newtime.Format(TIME_FORMAT_FULL_1), nil
}

func TimeUTC() (uint32, string, error) {

	zone, err := time.LoadLocation(ZONE_UTC)
	if err != nil {
		return 0, "", err
	}

	utc := time.Now().UTC()
	newtime := utc.In(zone)

	now := uint32(newtime.Unix()) - 8*3600
	return now, newtime.Format(TIME_FORMAT_FULL_1), nil
}

func TimeFormat(timestamp uint32, timeFormat string) (string, error) {
	var utc time.Time
	if timestamp == 0 {
		utc = time.Now().UTC()
	} else {
		utc = time.Unix(int64(timestamp), 0).UTC()
	}

	if len(timeFormat) == 0 {
		timeFormat = TIME_FORMAT_FULL_1
	}

	zone, err := time.LoadLocation(ZONE_ASIA_TAIPEI)
	if err != nil {
		return "", err
	}

	nowTime := utc.In(zone)

	return nowTime.Format(timeFormat), nil
}

func Time(dateFormat string) string {
	currentTime := time.Now().Local()

	if len(dateFormat) == 0 {
		return currentTime.Format(TIME_FORMAT_DATE_4)
	}

	return currentTime.Format(dateFormat)
}

func SecondToMinutes(sec uint32) (uint32, uint32) {

	return sec / uint32(60), sec % uint32(60)
}
